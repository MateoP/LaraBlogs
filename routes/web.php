<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\BlogController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\CommentController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* Common resource routes:
  index - show all items
  show - show single item
  create - show form to create new item
  store - store new item
  edit - show form to edit item
  update - update item
  destroy - delete item
 */

/* ###########################
 * # TODO USE MIDDLEWARE!!!! #
 * ###########################
 */

/* Index latest blogs */
Route::get('/', [BlogController::class, 'index']);

/* Show a blog creation form */
Route::get('/blogs/create', [BlogController::class, 'create'])
  ->middleware('auth');

/* Store a new blog */
Route::post('/blogs/store', [BlogController::class, 'store'])
  ->middleware('auth');

/* Show blog management page */
Route::get('/blogs/manage', [BlogController::class, 'manage'])
  ->middleware('auth');

/* Update the blog */
Route::put('/blogs/{blog}', [BlogController::class, 'update'])
  ->middleware('auth');

/* Show edit blog form */
Route::get('/blogs/{blog}/edit', [BlogController::class, 'edit'])
  ->middleware('auth');

/* Delete the blog */
Route::delete('/blogs/{blog}', [BlogController::class, 'delete'])
  ->middleware('auth');

/* Show a blog */
Route::get('/blogs/{blog}', [BlogController::class, 'show']);



/* Show the create user form */
Route::get('/user/register', [UserController::class, 'create'])
  ->middleware('guest');

/* Store the newly created user */
Route::post('/user/create', [UserController::class, 'store'])
  ->middleware('guest');

/* Log user out */
Route::post('/user/logout', [UserController::class, 'logout'])
  ->middleware('auth');

/* Show the login form */
Route::get('/user/login', [UserController::class, 'login'])
  ->name('login')->middleware('guest');

/* Authenticate the user */
Route::post('/user/authenticate', [UserController::class, 'authenticate'])
  ->middleware('guest');


/* Comment on a blog */
Route::post('/blogs/{blog}/comment', [CommentController::class, 'store'])
  ->middleware('auth');

























