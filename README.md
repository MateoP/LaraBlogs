# LaraBlogs

This is a blog website made with Laravel/PHP/Blade, HTML and CSS.

Features:
- view, create, edit and delete blogs
- view and create comments
- create accounts
- search by title, text or tags

![homepage.png](images/homepage.png)
![register.png](images/register.png)
![login.png](images/login.png)
![blog-showcase-1.png](images/blog-showcase-1.png)
![blog-showcase-2.png](images/blog-showcase-2.png)
![create-blog.png](images/create-blog.png)
![manage-blogs.png](images/manage-blogs.png)
Editing of the blogs is visually same as creating them.

License: 0BSD
