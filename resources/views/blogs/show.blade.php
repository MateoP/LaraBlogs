<x-layout>
  <br>
  <div class="blog-header">
    <p>{{$blog->title}}</p>
  </div>
  <div class="blog-text">
    <span>{{$blog->text}}</span>
    <p>- {{$blog->user_name}}<br>   Last updated: {{$blog->updated_at}} UTC</p>
    <x-blog-tags :tagsComb="$blog->tags"/>
  </div>
  <div class="comments">
    <h2>Comments</h2>
    <form method="POST" action="/blogs/{{$blog->id}}/comment">
      @csrf
      <textarea class="comments" type="text" name="comment"
        >{{old('comment')}}</textarea>
      @error ('comment')
      <p class="lr">{{$message}}</p>
      @enderror
      <button type="submit">Comment</button>
    </form>
    @unless (!count($comments))
    @foreach ($comments as $comment)
    <p id="comment-author">{{$comment->user_name}}</p>
    <p id="comment-date">{{$comment->updated_at}}</p>
    <pre>{{$comment->comment}}</pre>
    @endforeach
    @else
    <p> No comments on this blog. </p>
    @endunless
  </div>
</x-layout>
