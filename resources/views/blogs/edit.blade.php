<x-layout>
  <div class="cb-center-container">
    <h1>Edit blog</h1>
    <div class="mod-blog-container">
      <form class="mod-blog" method="POST" action="/blogs/{{$blog->id}}">
        @csrf
        @method ('PUT')
        <div class="fg-mod-blog">
          <label for="title">Title:</label>
          <input type="text" name="title"
            value="{{$blog->title}}"/>

          @error ('title')
          <p class="lr">{{$message}}</p>
          @enderror
        </div>

        <div class="fg-mod-blog">
          <label for="tags">Tags:</label>
          <input type="text" name="tags"
            value="{{$blog->tags}}"
            placeholder="Separate with ','"/>

          @error ('tags')
          <p class="lr">{{$message}}</p>
          @enderror
        </div>

        <div class="fg-mod-blog">
          <label for="text">Text:</label>
          <textarea type="text" name="text">{{$blog->text}}</textarea>

          @error ('text')
          <p class="lr">{{$message}}</p>
          @enderror
        </div>

        <div class="fg-mod-blog">
          <button type="submit">Update blog</button>
        </div>
      </form>
    </div>
  </div>
</x-layout>
