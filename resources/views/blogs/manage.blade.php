<x-layout>
  <h1>{{$user->name}}'s blogs</h1>
  
  <ul class="blogs">
    @unless ($blogs->isEmpty())

    <?php $blogCount=count($blogs);?>
    <p>Total blogs: <?php echo $blogCount; ?></p>
    
    @foreach ($blogs as $blog)
    <br>
    <div id="manage-blog-header">
      <p><a href="/blogs/{{$blog->id}}/edit">{{$blog->title}}</a>
      <form method="POST" action="/blogs/{{$blog->id}}">
        @csrf
        @method ('DELETE')
        <button>Delete</button>
      </form>
    <div>
    @endforeach
    @else
    
    <p>There are no blogs to list. <a href="/blogs/create">Create one?</a></p>
  
    @endunless
  </ul>
</x-layout>
