<x-layout>
  <h1>Latest blogs</h1>
  
  @include ('partials._search')
  
  <ul id="blogs">
    @unless (!count($blogs))
    
    @foreach ($blogs as $blog)
    <br>
    <div class="index-blog-header">
      <p><a href="/blogs/{{$blog->id}}">{{$blog->title}}</a>
      <br>written by {{$blog->user_name}}</p>
      <x-blog-tags :tagsComb="$blog->tags"/>
    </div>
    @endforeach
  
    @else
    
    <p>There are no blogs to list. <a href="/blogs/create">Create one?</a></p>
  
    @endunless
  </ul>
</x-layout>
