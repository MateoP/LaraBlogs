<x-layout>
  <div class="center-container">
    <h1>Register</h1>
    <div class="register-container">
      <form class="cred-form" method="POST" action="/user/create">
        @csrf
        <div class="fg">
          <label for="name">Username:</label>
          <input type="text" name="name"
            value="{{old('name')}}"/>

          @error ('name')
          <p class="lr">{{$message}}</p>
          @enderror
        </div>
        <div class="fg">
          <label for="email">Email:</label>
          <input type="text" name="email"
            value="{{old('email')}}"/>

          @error ('email')
          <p class="lr">{{$message}}</p>
          @enderror
        </div>

        <div class="fg">
          <label for="password">Password:</label>
          <input type="password" name="password"/>
        </div>
        <div class="fg">
          <label for="password_confirmation">Confirm password:</label>
          <input type="password" name="password_confirmation"/>

          @error ('password')
          <p class="lr">{{$message}}</p>
          @enderror
        </div>

        <div class="fg">
          <p id="dark">Already have an account? <a href="/user/login"
            >Login</a></p>
          <button type="submit">Register</button>
        </div>
      </form>
    </div>
  </div>
</x-layout>
