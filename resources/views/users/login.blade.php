<x-layout>
  <div class="center-container">
    <h1>Login</h1>
    <div class="login-container">
      <form class="cred-form" method="POST" action="/user/authenticate">
        @csrf
        <div class="fg">
          <label for="email">Email:</label>
          <input type="text" name="email"
            value="{{old('email')}}" />

          @error ('email')
          <p class="lr">{{$message}}</p>
          @enderror
        </div>

        <div class="fg">
          <label for="password">Password:</label>
          <input type="password" name="password"/>

          @error ('password')
          <p class="lr">{{$message}}</p>
          @enderror
        </div>
        <div class="fg">
          <p>Don't have an account? <a href="/user/register">Register</a></p>
          <button type="submit">Login</button>
        </div>
      </form>
    </div>
  </div>
</x-layout>
