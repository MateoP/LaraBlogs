<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" type="text/css" href="{{asset('css/style.css')}}">
  <title>LaraBlogs</title>
</head>

<body>
  <nav class="nav-panel">
    <ul>
      <li><a class="header" href="/">Home</a></li>
      @auth
      <li>
        <span>
        </span>
      </li>
      
      <li><a class="create-blog" href="/blogs/create">Create blog</a></li>

      <li><a class="manage-blogs" href="/blogs/manage">Manage</a></li>

      <li>
        <form method="POST" action="/user/logout">
          @csrf
          <button class="btn1" type="submit">
            <span> Logout  ({{auth()->user()->name}}) </span>
          </button>
        </form>
      </li>

      @else
      <li><a href="/user/login">Login</a></li>
      <li><a href="/user/register">Register</a></li>
      @endauth
    </ul>
  </nav>
 
  <main>
    {{$slot}}
  </main>
</body>
</html>
