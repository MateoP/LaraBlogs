@props (['tagsComb'])
@php
  $tags = explode(',', $tagsComb);
@endphp

<ul class="tags">
  @foreach ($tags as $tag)
  <li class="tag"><a href="/?tag={{strtolower($tag)}}">{{$tag}}</a></li>
  @endforeach
</ul>
