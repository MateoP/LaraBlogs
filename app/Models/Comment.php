<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
  use HasFactory;
  protected $fillable = ['blog_id', 'user_name', 'user_id', 'comment'];


  /* Relationship to author */
  public function user() {
    return $this->belongsTo(User::class, ['user_id', 'user_name']);
  }


  /* Relationship to blog */
  public function blog() {
    return $this->belongsTo(Blog::class, ['blog_id']);
  }
}
