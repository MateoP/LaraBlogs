<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Blog extends Model
{
    use HasFactory;
    protected $fillable = ['title', 'text', 'user_id', 'user_name', 'tags'];


    /* Search result */
    public function scopeFilter($query, array $filters) {
      if ($filters['tag'] ?? false) {
        $query->where('tags', 'like', '%'.request('tag').'%');
      }
      if ($filters['search'] ?? false) {
        $query->where('title', 'like', '%'.request('search').'%')
        ->orWhere('text', 'like', '%'.request('search').'%')
        ->orWhere('tags', 'like', '%'.request('search').'%');
      }
    }


    /* Relationship to comment */
    public function comment() {
      return $this->hasMany(Comment::class, ['blog_id']);
    }


    /* Relationship to user */
    public function user() {
      return $this->belongsTo(User::class, ['user_id']);
    }

}
