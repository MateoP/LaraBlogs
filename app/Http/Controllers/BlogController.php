<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Blog;
use App\Models\Comment;

class BlogController extends Controller
{
  /* List latest blogs */
  public function index() {
    return view('blogs.index', [
    'blogs' => Blog::latest('updated_at')->filter(request(['tag', 'search']))
      ->paginate(10)
    ]);
  }


  /* Show a single blog */
  public function show(Blog $blog) {
    return view('blogs.show', [
      'blog' => $blog,
      'comments' => Comment::where('blog_id', 'like', $blog->id)
      ->latest('updated_at')->get()
    ]);
  }


  /* Show blog creation form */
  public function create() {
    return view('blogs.create');
  }

  
  /* Create the blog */
  public function store(Request $request) {
    $formFields = $request->validate([
      'title' => ['required', 'min:4', 'max:100'],
      'text' => ['required', 'min:30', 'max: 8192'],
      'tags' => ['required', 'min:3', 'max:200']
    ]);

    $formFields['tags'] = str_replace(' ', '',$formFields['tags']);
    $formFields['tags'] = strtolower($formFields['tags']);
    $formFields['user_id'] = auth()->id();
    $formFields['user_name'] = auth()->user()->name;
    
    Blog::create($formFields);

    return redirect('/')->with('comment', 'A new blogs has been created!');
  }


  /* Show management page */
  public function manage(Blog $blog) {
    return view('blogs.manage', [
    'user' => auth()->user(),
    'blogs' => Blog::latest('updated_at')->where('user_id', auth()->id())
      ->get()
    ]);
  }


  /* Show user the blog edit form */
  public function edit(Blog $blog) {
    return view('blogs.edit', [
      'blog' => $blog,
    ]);
  }

  
  /* Update the blog */
  public function update(Request $request, Blog $blog) {
    if ($blog->user_id != auth()->id())
      abort(403, 'Unauthorized action!');

    $formFields = $request->validate([
      'title' => ['required', 'min:4', 'max:100'],
      'text' => ['required', 'min:30', 'max:8192'],
      'tags' => ['required', 'min:3', 'max:200']
    ]);

    $formFields['tags'] = str_replace(' ', '',$formFields['tags']);
    $formFields['tags'] = strtolower($formFields['tags']);
    $blog->update($formFields);

    return redirect('/blogs/'.$blog->id)
      ->with('comment', 'Blog updated successfully');
  }

  
  /* Delete the blog */
  public function delete(Blog $blog) {
    if ($blog->user_id != auth()->id())
      abort(403, 'Unauthorized action!');

    $blog->delete();
    return back()->with('message', 'Blog has been deleted successfully');
  }
}
