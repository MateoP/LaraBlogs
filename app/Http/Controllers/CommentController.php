<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Blog;
use App\Models\Comment;

class CommentController extends Controller
{
  /* Create a new comment */
  public function store(Request $request, Blog $blog) {
    $formFields = $request->validate([
      'comment' => ['required', 'min:6', 'max:1024']
    ]);

    $formFields['blog_id'] = $blog->id;
    $formFields['user_id'] = auth()->id();
    $formFields['user_name'] = auth()->user()->name;

    Comment::create($formFields);

    return back()->with('message', 'Successfully commented');
  }
}
