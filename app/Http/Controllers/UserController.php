<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Models\User;

class UserController extends Controller
{
  /* Show user the registration form */
  public function create() {
    return view('users.register');
  }


  /* Create a new user */
  public function store(Request $request) {
    $formFields = $request->validate([
      'name' => ['required', 'min:3', 'max:30'],
      'email' => ['required', 'email', 'max:30',
        Rule::unique('users', 'email')],
      'password' => 'required|confirmed|min:6|max:512'
    ]);

    /* Hash the password */
    $formFields['password'] = bcrypt($formFields['password']);

    /* Create the new user */
    $user = User::create($formFields);

    /* Automatically login the user, too */
    auth()->login($user);

    return redirect('/')->with('message',
      'New user has been created and is logged in.');
  }


  /* Log user out */
  public function logout(Request $request) {
    auth()->logout();

    $request->session()->invalidate();
    $request->session()->regenerateToken();

    return redirect('/')->with('message', 'You have been logged out.');
  }


  /* Show use the login form */
  public function login() {
    return view("users.login");
  }


  /* Authenticate the user */
  public function authenticate(Request $request) {
    $formFields = $request->validate([
      'email' => ['required', 'email'],
      'password' => 'required'
    ]);

    if (auth()->attempt($formFields)) {
      $request->session()->regenerate();
      return redirect('/')->with('message', 'Login successful!!');
    }

    return back()->withErrors(['password' => 'Invalid credentials!'])
      ->onlyInput('password');
  }
}
