<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Blog;
use App\Models\Comment;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
      $userList = User::factory(10)->create();

      for ($i=0; $i<10; $i++) {
        $blog = Blog::factory()->create([
          'user_id' => $userList[$i]->id,
          'user_name' => $userList[$i]->name,
          'tags' => 'latin,personal,rant'
        ]);

        for ($j=0; $j<10; $j++) {
          Comment::factory()->create([
            'blog_id' => $blog->id,
            'user_id' => $userList[$j]->id,
            'user_name' => $userList[$j]->name,
          ]);

          // \App\Models\User::factory()->create([
          //     'name' => 'Test User',
          //     'email' => 'test@example.com',
          // ]);
        }
      }
    }
}
